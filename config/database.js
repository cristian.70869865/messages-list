/**
 * Created by BURGOS on 23/05/2018.
 */
const mongoose = require('mongoose');
const dbName   = 'messages-list';

module.exports = {
  connect: () => mongoose.connect('mongodb://localhost/' + dbName),
  dbName,
  connection: () => {
  if(mongoose.connection)
    return mongoose.connection;
  return this.connect();
  }
};