/**
 * Created by BURGOS on 23/05/2018.
 */

const express = require('express');
const Application = require('./app');
const application = new Application(express).getApplication();
const db = require('./config/database');

db.connect();
application.listen(process.env.PORT || 500, () => {
  console.log('API Rest corriendo.')
});