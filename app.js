/**
 * Created by BURGOS on 23/05/2018.
 */

class Application {
  constructor (express) {
    this.app = express();
    this.bodyParser = require('body-parser');
  }

  getApplication () {
    this.app.use(this.bodyParser.urlencoded({extended: false}));
    this.app.use(this.bodyParser.json());
    this.defineCors();
    return this.app;
  }

  defineCors () {
    this.app.all('/*', (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, " +
        "Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
      res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
      next();
    });
  }
}

module.exports = Application;